#!/usr/bin/env sh
# Cache SMD json for ships
mkdir public

for i in 2 3 7 8 9 6; do
    echo "Getting $i"
    curl -H "XF-Api-Key: RSVcV-pNXnzaZgHTths0Qd11WsNJ_EK7" -H "Content-type: application/x-www-form-urlencoded" https://starmadedock.net/cached-api/resource-categories/$i.json.gz > public/$i.json.gz
done

echo "Done!"